using System;

namespace UnityLogging
{
	public class Calc : ICalc
	{
		public int Add(int a, int b)
		{
			return a + b;
		}
		public int Sub(int a, int b)
		{
			return a - b;
		}
	}
}
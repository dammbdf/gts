using System;

namespace UnityLogging
{
    interface ICalc
    {
		int Add(int a, int b);
		int Sub(int a, int b);
    }
}
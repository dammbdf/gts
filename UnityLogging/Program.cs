﻿using System;
using Unity;
using Unity.Interception;
using Unity.Interception.Interceptors.InstanceInterceptors.InterfaceInterception;

namespace UnityLogging
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");

			var container = ConfigLogging();
			ICalc calc = container.Resolve<ICalc>();

			var plus = calc.Add(12, 1);
			var minus = calc.Sub(2, 4);
		}

		static IUnityContainer ConfigLogging()
		{
			var container = new UnityContainer();

			container.AddNewExtension<Interception>();

			container.RegisterType<ICalc, Calc>()
				.Configure<Interception>()
				.SetInterceptorFor<ICalc>(new InterfaceInterceptor());

			//DependencyResolver.SetResolver(new UnityServiceLocator(container));

			return container;
		}
	}
}

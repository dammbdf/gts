# Today's Exchange Rate

GTSRo is looking to provide the best exchange rate to its customers, enhancing the overall user experience.

## [Specs][specs]

### [Requirements][reqs]

## [Analysis][analysis]

### [IssuesBoard][issues]

## [Application][code]

Please build from [source][code]!

To get it started drop [settings.json][context] in your current working directory ([CWD][cwd]) - run the app without the file and look in the console for the path where it's expected... [... linux mf]

e.g. console:
```
__ Context is corrupted! To use default, try creating one at [/home/damm/Work/gts/settings.json] . @ tooCommon
   at tooCommon.context.ContextBase`1.Load(String fileName) in /home/damm/Work/gts/tooCommon/context/ContextBase.cs:line 69
   at presenter.Program.Main(String[] args) in /home/damm/Work/gts/presenter/Program.cs:line 22
___ _ No context available in the expected path [/home/damm/Work/gts/settings.json] ! @ tooCommon
```




<!-- Links -->
[specs]: https://gitlab.com/dammbdf/gts/-/wikis/1_Specs
[analysis]: https://gitlab.com/dammbdf/gts/-/wikis/2_Analysis
[reqs]: https://gitlab.com/dammbdf/gts/-/requirements
[issues]: https://gitlab.com/dammbdf/gts/-/boards
[code]: https://gitlab.com/dammbdf/gts/-/tree/master
[context]:https://gitlab.com/dammbdf/gts/-/blob/master/settings.json
[cwd]:https://en.wikipedia.org/wiki/Working_directory
using System;
using System.Threading.Tasks;
using tooCommon;

namespace exchange
{
	public static class RateFactory
	{
		public static async Task<IExchangeable> GetSourceRate(Source type, string URL)
		{
			switch (type)
			{
				case Source.S1:
					return await ApiCaller.RequestExchangeData<S1Model>(URL);
				case Source.S2:
					return await ApiCaller.RequestExchangeData<S2Model>(URL);
				default:
					throw new NotSupportedException($"Context for ApisSource.Type [{type}] is unknown!");
			}
		}
	}
}

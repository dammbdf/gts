﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace exchange
{
	public static class ApiCaller
	{
		public static async Task<T> RequestExchangeData<T>(string url)
		{
			using (var client = new HttpClient())
			{
				try
				{
					client.DefaultRequestHeaders.Accept.Clear();
					client.DefaultRequestHeaders.Accept.Add(
						new MediaTypeWithQualityHeaderValue("application/json"));

					var streamTask = client.GetStreamAsync(url);

					var rate = await JsonSerializer.DeserializeAsync<T>(await streamTask);

					return rate;
				}
				catch (Exception ex)
				{
					if (ex is HttpRequestException)
					{
						throw new Exception(
							$"_ Web-service call failed! Is service at [{url}] GET-able in a browser ?",
							ex
						);
					}
					else if (ex is JsonException || ex is NotSupportedException)
					{
						throw new Exception(
							$"_ Deserializing to JSON failed! Does service at [{url}] replies with valid JSON ?",
							ex
						);
					}

					throw;
				}
			}
		}
	}
}

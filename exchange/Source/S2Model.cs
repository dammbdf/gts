using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json.Serialization;

namespace exchange
{
	public class S2Model : IExchangeable
	{
		// --- Interface implementation ---------
		public RateModel GetCurrentXrate
		{
			get
			{
				if (Coefficients == null)
				{
					throw new NullReferenceException("_ Service call or deserialization of data failed!");
				}
				else if (Coefficients.EUR == 0)
				{
					throw new NullReferenceException("_ Currency ration [0] indicates it's not available in the data!");
				}

				var result = new RateModel()
					.OnDate(DateTime.UtcNow)
					.From(Currency.USD).To(Currency.EUR)
					.WithValue(Coefficients.EUR);

				return result;
			}
		}

		// --- POCO -----------------------------

		[JsonPropertyName("timestamp")]
		public int TimeStamp { get; set; }

		[JsonPropertyName("base")]
		public string FromCurrency { get; set; }

		[JsonPropertyName("rates")]
		public Rates Coefficients { get; set; }

		public class Rates
		{
			[JsonPropertyName("EUR")]
			public decimal EUR { get; set; }

			[JsonPropertyName("USD")]
			public decimal USD { get; set; }

		}
	}
}

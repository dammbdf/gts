using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json.Serialization;

namespace exchange
{
	public class S1Model : IExchangeable
	{
		// --- Interface implementation ---------
		public RateModel GetCurrentXrate
		{
			get
			{
				if (!Successful || Coefficients == null)
				{
					throw new NullReferenceException("_ Service call or deserialization of data failed!");
				}
				else if (Coefficients.EUR == 0)
				{
					throw new NullReferenceException("_ Currency ratio [0] indicates it's not available in the data!");
				}

				var result = new RateModel()
					.OnDate(DateTime.UtcNow)
					.From(Currency.USD).To(Currency.EUR)
					.WithValue(Coefficients.EUR);

				return result;
			}
		}

		// --- POCO -----------------------------

		[JsonPropertyName("success")]
		public bool Successful { get; set; }

		[JsonPropertyName("timestamp")]
		public int TimeStamp { get; set; }

		[JsonPropertyName("source")]
		public string FromCurrency { get; set; }

		[JsonPropertyName("quotes")]
		public Quotes Coefficients { get; set; }

		public class Quotes
		{
			[JsonPropertyName("USDEUR")]
			public decimal EUR { get; set; }

			[JsonPropertyName("USDUSD")]
			public decimal USD { get; set; }

		}
	}
}


namespace exchange
{
	public interface IExchangeable
	{
		RateModel GetCurrentXrate { get; }
	}
}
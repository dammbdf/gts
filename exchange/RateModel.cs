using System;

namespace exchange
{
	public class RateModel
	{
		public DateTime Date { get; private set; } = DateTime.UtcNow;
		public Currency Source { get ; private set; } = Currency.USD;
		public Currency Target { get; private set; } = Currency.EUR;
		public decimal Ratio { get; private set; } = 1;

		public RateModel() { }
		public RateModel(DateTime date, Currency sourceCurrency, Currency targetCurrency, decimal ratio)
		{
			Date = date;
			Source = sourceCurrency;
			Target = targetCurrency;
			Ratio = ratio;
		}

		public RateModel OnDate(DateTime date)
		{
			Date = date;
			return this;
		}
		public RateModel OnDate(string date)
		{
			Date = DateTime.Parse(date);
			return this;
		}
		public RateModel From(Currency sourceCurrency)
		{
			Source = sourceCurrency;
			return this;
		}
		public RateModel To(Currency targetCurrency)
		{
			Target = targetCurrency;
			return this;
		}
		public RateModel WithValue(decimal ratio)
		{
			Ratio = ratio;
			return this;
		}

		public override string ToString()
		{
			var dateStr = Date.ToString("yyyy/MM/dd HH:mm:ss UTC");

			return $"{Source} -> {Target} : {Ratio} | {dateStr}";
		}

		public string ToString2Lines()
		{
			var dateStr = Date.ToString("yyyy/MM/dd HH:mm:ss UTC");

			return $"{Source} -> {Target}     {Ratio} {Environment.NewLine}{dateStr}";
		}

		public string ToStringFancy()
		{
			return $@"
			On {Date}, the
			{Source} to {Target} x-rate is {Ratio}
			";
		}
	}
}

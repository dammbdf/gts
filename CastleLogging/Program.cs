﻿using System;
using Castle.MicroKernel;
using Castle.Windsor;

namespace CastleLogging
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");

			var container = ConfigLogging();
			ICalc calc = container.Resolve<ICalc>();

			var plus = calc.Add(12, 1);
			var minus = calc.Sub(2, 4);
		}

		static WindsorContainer ConfigLogging()
		{
			var container = new WindsorContainer();
			//MeasureDurationInterceptorRegistrar.Initialize(container.Kernel);

			container. .ComponentModel.Interceptors.Add(
				new InterceptorReference(typeof(MeasureDurationInterceptor)));


			return container;
		}



	}
}

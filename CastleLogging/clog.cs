using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Castle.Core;
using Castle.Core.Logging;
using Castle.DynamicProxy;
using Castle.MicroKernel;

namespace CastleLogging
{
	public class MeasureDurationInterceptor : IInterceptor
	{
		public ILogger Logger { get; set; }

		public MeasureDurationInterceptor()
		{
			Logger = NullLogger.Instance;
		}

		public void Intercept(IInvocation invocation)
		{
			//Before method execution
			var stopwatch = Stopwatch.StartNew();

			//Executing the actual method
			invocation.Proceed();

			//After method execution
			stopwatch.Stop();
			Logger.InfoFormat(
				"MeasureDurationInterceptor: {0} executed in {1} milliseconds.",
				invocation.MethodInvocationTarget.Name,
				stopwatch.Elapsed.TotalMilliseconds.ToString("0.000")
				);
		}
	}

	public static class MeasureDurationInterceptorRegistrar
	{
		public static void Initialize(IKernel kernel)
		{
			kernel.ComponentRegistered += Kernel_ComponentRegistered;
		}

		private static void Kernel_ComponentRegistered(string key, IHandler handler)
		{
			if (typeof(ICalc).IsAssignableFrom(handler.ComponentModel.Implementation))
			{
				handler.ComponentModel.Interceptors.Add(
					new InterceptorReference(typeof(MeasureDurationInterceptor)));
			}
		}
	}

	public class clog
	{


		// public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
		// {
		// 	//throw new NotImplementedException();
		// 	Console.WriteLine();
		// 	Console.WriteLine();
		// 	var classMethodName = string.Format("{0}::{1}", input.MethodBase.ReflectedType.Name, input.MethodBase.Name);
		// 	Console.WriteLine("*** Before {0} method exec ***", classMethodName);
		// 	Console.WriteLine(GetParametersInfo(input));

		// 	IMethodReturn mret;
		// 	try
		// 	{
		// 		var sw = new Stopwatch();
		// 		sw.Start();

		// 		mret = getNext()(input, getNext);

		// 		sw.Stop();
		// 		Console.WriteLine("Exec time for {0} was {1}", classMethodName, sw.Elapsed);

		// 		Console.WriteLine("Returned value from method {0} is {1}.", classMethodName, mret.ReturnValue.ToString());
		// 		Console.WriteLine("Exiting the TRY block of {0} method. ", classMethodName);
		// 	}
		// 	catch (Exception ex)
		// 	{
		// 		Console.WriteLine("--> CATCH block of {0} method with error {1}", classMethodName, ex.ToString());
		// 		throw;
		// 	}
		// 	finally
		// 	{
		// 		Console.WriteLine("*** FINALLY block of {0} method ***", classMethodName);
		// 	}

		// 	return mret;
		// }

		// private string GetParametersInfo(IMethodInvocation input)
		// {
		// 	var mret = new StringBuilder();

		// 	for (int i = 0; i < input.Arguments.Count; i++)
		// 	{
		// 		mret.AppendLine($"{input.Arguments.GetParameterInfo(i).Name} : {input.Arguments[i]}");
		// 		return mret.ToString();
		// 	}
		// 	return mret.ToString();
		// }
	}
}
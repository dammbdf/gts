namespace presenter
{
	internal class MessageBoxFactory : IPresenterFactory
	{
		public IPresenter Create()
		{
			return new MessageBoxPresenter();
		}
	}
}
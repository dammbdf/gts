using System;
using System.Text;

namespace presenter
{
	public class MessageBoxPresenter : IPresenter
	{
		const string FLOOR = "|==================================================|";
		const string WALLS = "|                                                  |";

		public int IndentSize => 4;
		public int Margin => 1;

		public void Show(string message)
		{
			var ind = new String(' ', IndentSize);
			var m = new String(' ', Margin);

			var sb = new StringBuilder();

			sb.AppendLine()
				.Append(ind).AppendLine(FLOOR)
				.Append(ind).AppendLine(WALLS)
				.AppendLine();

			sb.Append(ind).Append(m)
				.AppendJoin(Environment.NewLine + ind + m, message.Split(Environment.NewLine))
				.AppendLine();

			sb.AppendLine()
				.Append(ind).AppendLine(WALLS)
				.Append(ind).AppendLine(FLOOR)
				.AppendLine();

			Console.WriteLine(sb);
		}
	}
}
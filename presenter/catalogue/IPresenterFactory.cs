namespace presenter
{
	internal interface IPresenterFactory
	{
		IPresenter Create();
	}
}
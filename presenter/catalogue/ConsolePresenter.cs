using System;

namespace presenter
{
	public class ConsolePresenter : IPresenter
	{
		public void Show(string message)
		{
			Console.WriteLine(message);
		}
	}
}
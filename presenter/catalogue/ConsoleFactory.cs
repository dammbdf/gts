namespace presenter
{
	internal class ConsoleFactory : IPresenterFactory
	{
		public IPresenter Create()
		{
			return new ConsolePresenter();
		}
	}
}
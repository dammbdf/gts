namespace presenter
{
	interface IPresenter
	{
		void Show(string message);
	}
}
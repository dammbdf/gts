﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using exchange;
using tooCommon;
using tooCommon.context;

namespace presenter
{
	class Program
	{
		static async Task Main(string[] args)
		{
			//await testExchange(); // TODEL
			//testContext(); // TODEL
			//testPresenter(); // TODEL

			try
			{

				var c = Context.Load();

				Show(c.PrintMode,
					BestExchange(
						await FromSources(c.ApiSources)
					)
				);

			}
			catch (Exception ex)
			{
				Console.WriteLine($@"_{ex.Message} @ {ex.Source}");
				Console.WriteLine(ex.StackTrace);

				if (ex.InnerException != null)
				{
					Console.WriteLine($@"___ {ex.InnerException.Message} @ {ex.InnerException.Source}");
				}
			}
		}
		private static void Show(PresenterType presenterType, (decimal buy, decimal sell) bestRate)
		{
			IPresenterFactory pFactory;
			switch (presenterType)
			{
				case PresenterType.Console:
					pFactory = new ConsoleFactory();
					break;
				case PresenterType.MessageBox:
					pFactory = new MessageBoxFactory();
					break;
				default:
					throw new System.NotImplementedException();
			}

			var p = pFactory.Create();
			p.Show($" >Buy @ {bestRate.buy} USD/EUR {Environment.NewLine} Sell @ {bestRate.sell} USD/EUR");
		}

		private static (decimal buy, decimal sell) BestExchange(List<RateModel> rates)
		{
			if (rates == null || rates.Count == 0)
			{
				throw new Exception("_ No valid data available within the configured sources!");
			}

			var b = rates.Max(r => r.Ratio);
			var s = rates.Min(r => r.Ratio);

			return (buy: b, sell: s);
		}

		private static async Task<List<RateModel>> FromSources(List<ApiSource> sources)
		{
			if (sources == null || sources.Count == 0)
			{
				throw new Exception("_ No sources configured to supply currency exchange rates!");
			}

			var rates = new List<RateModel>();
			foreach (var s in sources)
			{
				try
				{
					var r = await RateFactory.GetSourceRate(s.Type, s.URL);
					rates.Add(r.GetCurrentXrate);
				}
				catch (Exception ex)
				{
					Console.WriteLine($@"_{ex.Message} @ {ex.Source} -> removing source and continue...");
				}
			}

			return rates;
		}

		// --- Tests --- // TODEL
		private static void testPresenter()
		{
			IPresenter p1 = null;
			p1 = new ConsolePresenter();
			p1.Show("Something in plain Console");

			p1 = new MessageBoxPresenter();
			p1.Show(@"	Something in a MessageBox
		! mind the border...");
		}

		private static void testContext()
		{
			var c = Context.Load();

			// c.ApiSources.Add(new ApiSource
			// {
			// 	Type = Source.S1,
			// 	URL = @"http://www.apilayer.net/api/live?access_key=6653c3ea32932753a5c4a956ddc7de27"
			// });

			// c.ApiSources.Add(new ApiSource
			// {
			// 	Type = Source.S2,
			// 	URL = @"https://openexchangerates.org/api/latest.json?app_id=1de86dfd996b4c9da20c0b3fa6eefaa4&base=USD"
			// });

			// Context.Save(c);

		}

		static async Task testExchange()
		{
			var r1 = await RateFactory.GetSourceRate(Source.S1, FirstURL);
			Console.WriteLine(r1.GetCurrentXrate);

			IExchangeable r2 = await RateFactory.GetSourceRate(Source.S2, SecondURL);
			Console.WriteLine(r2.GetCurrentXrate);

		}

		const string FirstURL =
			@"http://www.apilayer.net/api/live?access_key=6653c3ea32932753a5c4a956ddc7de27";
		const string SecondURL =
			@"https://openexchangerates.org/api/latest.json?app_id=1de86dfd996b4c9da20c0b3fa6eefaa4&base=USD";

	}
}

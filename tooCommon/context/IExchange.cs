﻿using System.Collections.Generic;

namespace tooCommon.context
{
	public interface IExchange
	{
		List<ApiSource> ApiSources { get; set; }
	}
}

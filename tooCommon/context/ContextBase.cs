﻿using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace tooCommon.context
{
	public class ContextBase<T> where T : new()
	{
		private const string DEFAULT_FILENAME = @"settings.json";

		private static JsonSerializerOptions Opt
		{
			get
			{
				var o = new JsonSerializerOptions
				{
					AllowTrailingCommas = true,
					IgnoreNullValues = true,
					IgnoreReadOnlyProperties = true,
					ReadCommentHandling = JsonCommentHandling.Skip,
					WriteIndented = true
				};

				o.Converters.Add(new JsonStringEnumConverter());

				return o;
			}
		}

		public static void Save(T pSettings, string fileName = DEFAULT_FILENAME)
		{
			try
			{
				File.WriteAllText(fileName, JsonSerializer.Serialize(pSettings, Opt));
			}
			catch (Exception ex)
			{
				var p = Path.GetFullPath(fileName);
				throw new Exception(
					$"_ Can't save the context! Do I have permission for location: [{p}] ?",
					ex);
			}
		}

		public static T Load(string fileName = DEFAULT_FILENAME)
		{
			try
			{
				var t = new T();

				if (File.Exists(fileName))
				{
					t = JsonSerializer.Deserialize<T>(File.ReadAllText(fileName), Opt);
				}
				else
				{
					var p = Path.GetFullPath(fileName);
					throw new Exception(
						$"_ No context available in the expected path [{p}] !"
					);
				}

				return t;
			}
			catch (Exception ex)
			{
				var p = Path.GetFullPath(fileName);
				throw new Exception(
					$"_ Context is corrupted! Try re-creating one at [{p}] .",
					ex);
			}
		}
	}
}

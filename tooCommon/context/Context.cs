﻿using System.Collections.Generic;

namespace tooCommon.context
{
	public class Context : ContextBase<Context>, IPresenter, IExchange
	{
		// --- Presenter settings ---------------
		public PresenterType PrintMode { get; set; } = PresenterType.Console;

		// --- Exchange settings ----------------
		public List<ApiSource> ApiSources { get; set; } = new List<ApiSource>();
	}
}

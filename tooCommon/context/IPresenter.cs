﻿namespace tooCommon.context
{
	public interface IPresenter
	{
		PresenterType PrintMode { get; set; } // val: Console [default], MessageBox
	}
}

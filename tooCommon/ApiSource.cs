namespace tooCommon
{
	public struct ApiSource
	{
		public Source Type { get; set; }
		public string URL { get; set; }
	}
}
